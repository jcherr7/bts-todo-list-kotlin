package com.example.bts_todo_list_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import android.content.ContentValues.TAG // alternative method in case you don't want to come up with your own tag

class MainActivity : AppCompatActivity() {

    private lateinit var listView: ListView
    private lateinit var dataList: List<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val TAG = "CalledThisTAGHere"

        //find list view
        listView = findViewById(R.id.list_view)

        //create list data
        dataList = listOf("Lorem ipsum dolor sit amet", "vidisse persecuti sententiae mei ad", "at facer homero facilisi sit", "Id qui imperdiet theophrastus", "dolor reformidans est eu", "Malis dolorum cum eu", "ne iudicabit pertinacia sententiae his", "Mel ornatus temporibus id", "Et atqui tollit constituto sit", "eam graece populo graeco id", "Magna consul id ius", "at nam nisl deseruisse", "Erant commodo dissentias est cu", "...and no, I do not speak Latin.", "Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and printing in place of English to emphasise design elements over content.", "it's not genuine, correct, or comprehensible Latin anymore.", "taken from https://generator.lorem-ipsum.info/")
        dataList.forEach{
            item -> Log.d(TAG,item) // prints out each item in the list on a new log line
        }

        //hook list data up to our list using array adapter
        val listAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, dataList)
        listView.adapter = listAdapter
    }
}